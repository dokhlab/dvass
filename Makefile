##### Please set the BIN_DIR variable #####
BIN_DIR   := bin
###########################################

FLAGS     := -std=c++11 -MMD
CC        := g++

ifeq (true,${DEBUG})
	BUILD_PREFIX := build/debug
	FLAGS        := $(FLAGS) -g -gdwarf-2
else
	BUILD_PREFIX := build/release
	FLAGS        := $(FLAGS) -DNDEBUG -O3
endif

all: checkdirs dvass

install: checkdirs dvass

dvass: $(BIN_DIR)/dvass

$(BIN_DIR)/dvass: $(BUILD_PREFIX)/dvass.o
	$(CC) $(FLAGS) $< -o $@

$(BUILD_PREFIX)/dvass.o: dvass.cpp
	$(CC) $(FLAGS) $< -c -o $@

#$(BIN_DIR)/dvass: dvass.cpp
#	$(CC) $(FLAGS) $< -o $@

.PHONY: all install checkdirs clean dvass

checkdirs: $(BUILD_DIR) $(BIN_DIR) $(BUILD_PREFIX)

$(BUILD_DIR) $(BIN_DIR) $(BUILD_PREFIX):
	@mkdir -p $@

clean:
	@rm -rf bin build

-include $(BUILD_PREFIX)/*.d

