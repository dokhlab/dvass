#include <vector>
#include <random>
#include <iomanip>
#include <array>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <utility>
#include <functional>
#include <fstream>

#define PI 3.1415927

using namespace std;

mt19937 rand_engine { 11 };
uniform_real_distribution<double> unif_distr { 0, 1 };
int flag = 1;

/**
 * Generate a random float number between 0 and 1.
 */
double my_rand() {
    return unif_distr(rand_engine);
}

/**
 * Tokenize a string.
 */
vector<string> string_tokenize(const string &str, const string &delimiters = " \t") {
    vector<string> tokens;
    auto lastPos = str.find_first_not_of(delimiters, 0);
    auto pos = str.find_first_of(delimiters, lastPos);
    while (string::npos != pos || string::npos != lastPos) {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
    return std::move(tokens);
}

/**
 * Parse a string to type T.
 */
template<typename T, typename U>
T parse(U && u) {
    stringstream stream;
    T t;

    stream << u;
    stream >> t;
    return t;
}

/**
 * 2D Matrix Class.
 */
struct Mat {
    double *data = NULL;
    array<int, 2> shape;

    Mat(int i, int j) {
        shape[0] = i;
        shape[1] = j;
        int size = i * j;
        data = new double[size];
    }

    Mat(const Mat &m) {
        if (data != NULL) {
            delete [] data;
        }

        shape = m.shape;
        int size = shape[0] * shape[1];
        data = new double[size];
        for (int i = 0; i < size; i++) {
            data[i] = m.data[i];
        }
    }

    Mat(Mat &&m) {
        swap(shape, m.shape);
        swap(data, m.data);
    }

    Mat &operator =(const Mat &m) {
        if (data != NULL) {
            delete [] data;
        }

        shape = m.shape;
        int size = shape[0] * shape[1];
        data = new double[size];
        for (int i = 0; i < size; i++) {
            data[i] = m.data[i];
        }
    }

    Mat &operator =(Mat &&m) {
        swap(shape, m.shape);
        swap(data, m.data);
    }

    ~Mat() {
        if (data != NULL) {
            delete [] data;
        }
    }

    int rows() const {
        return shape[0];
    }

    int cols() const {
        return shape[1];
    }

    double &operator ()(int i, int j) {
        return data[i * shape[1] + j];
    }

    const double &operator ()(int i, int j) const {
        return data[i * shape[1] + j];
    }

    void set_zero() {
        for (int i = 0; i < shape[0] * shape[1]; i++) {
            data[i] = 0;
        }
    }

    template<typename F_>
    void set_values(F_ &&f) {
        for (int i = 0; i < shape[0]; i++) {
            for (int j = 0; j < shape[1]; j++) {
                data[i * shape[1] + j] = f(i, j);
            }
        }
    }
};

/**
 * String format.
 */
template<typename... Pars_>
string string_format(string fmt, Pars_ && ...pars) {
    int count = snprintf(NULL, 0, fmt.c_str(), pars...);
    string buf;
    buf.resize(count);
    sprintf(&(buf[0]), fmt.c_str(), pars...);
    return move(buf);
}

/**
 * To print a matrix.
 */
ostream &operator <<(ostream &out, const Mat &m) {
    for (int i = 0; i < m.rows(); i++) {
        for (int j = 0; j < m.cols(); j++) {
            out << string_format("%8.3f", m(i, j));
        }
        out << endl;
    }
}

/**
 * Smooth the bound matrix by using triangle equality.
 */
template<typename T>
int triangle_smooth(T &&b) {
    int f = 0;
    int len = b.rows();
    double d;
    for (int i = 0; i < len; i++) for (int j = i + 1; j < len; j++) for (int k = j + 1; k < len; k++) {
        d = b(i, k) + b(j, k); if (b(i, j) > d) { b(i, j) = d; f++; } if (b(j, i) > d) throw "Wrong bound matrix!";
        d = b(i, j) + b(j, k); if (b(i, k) > d) { b(i, k) = d; f++; } if (b(k, i) > d) throw "Wrong bound matrix!";
        d = b(i, j) + b(i, k); if (b(j, k) > d) { b(j, k) = d; f++; } if (b(k, j) > d) throw "Wrong bound matrix!";
        d = b(k, i) - b(j, k); if (b(j, i) < d) { b(j, i) = d; f++; }
        d = b(k, j) - b(i, k); if (b(j, i) < d) { b(j, i) = d; f++; }
        d = b(j, i) - b(j, k); if (b(k, i) < d) { b(k, i) = d; f++; }
        d = b(k, j) - b(i, j); if (b(k, i) < d) { b(k, i) = d; f++; }
        d = b(j, i) - b(i, k); if (b(k, j) < d) { b(k, j) = d; f++; }
        d = b(k, i) - b(i, j); if (b(k, j) < d) { b(k, j) = d; f++; }
    }
    print_bound(b);
    return f;
}

/**
 * Smooth the bound matrix.
 */
void smooth(Mat &b) {
    // check bound matrix
    if (b.rows() == 0 || b.cols() == 0 || b.rows() != b.cols()) {
        cerr << string_format("Wrong bound matrix: (%d, %d)", b.rows(), b.cols()) << endl;
        exit(1);
    }

    // Main Loop
    int max_step = 300;
    while (triangle_smooth(b) != 0 && max_step-- > 0);
}

/**
 * Find the largest DV in a bound matrix.
 */
array<int, 2> find_max_dv(const Mat &b) {
    double max = b(0, 1) - b(1, 0);
    array<int, 2> ind {0, 1};
    for (int i = 0; i < b.rows(); i++) {
        for (int j = i + 1; j < b.cols(); j++) {
            double d = b(i, j) - b(j, i);
            if (d > max) {
                max = d;
                ind[0] = i;
                ind[1] = j;
            }
        }
    }
    return move(ind);
}

using Helix = vector<int>;
using Helices = vector<Helix>;

/**
 * Get the secondary structure level of a symbol.
 *
 * .:0, (:-1, ):1, [:-2, ]:2, {:-3, }:3
 */
int ss_level(const char &c) {
    static vector<array<char, 2>> bp_symbols {{'(', ')'}, {'[', ']'}, {'{', '}'}, {'<', '>'}};
    static vector<char> loop_symbols {'.', '-'};

    for (auto && symbol : loop_symbols) {
        if (c == symbol) {
            return 0;
        }
    }

    for (int i = 0; i < bp_symbols.size(); i++) {
        if (c == bp_symbols[i][0]) {
            return -1 - i;
        }
        else if (c == bp_symbols[i][1]) {
            return i + 1;
        }
    }

    return 0;
}

/**
 * Identify helices from a secondary structure.
 */
Helices identify_helices(const string &ss) {
    Helices helices;
    vector<vector<int>> all_levels(10);
    vector<vector<int>> all_indices(10);
//    vector<int> levels;
//    vector<int> indices;

    auto foo = [](vector<int> &indices, vector<int> &levels) -> int {
        int a = 0;
        int b = 0;
        for (int j = indices.size() - 1; j >= 0; j--) {
            if (levels[j] > 0) {
                b++;
            }
            else if (levels[j] < 0) {
                a++;
            }
            if (a == b) {
                return j;
            }
        }
    };

    auto extract_helix = [&helices, &foo](vector<int> &indices, vector<int> &levels){
        helices.push_back(vector<int>{});

        cout << "Helix: ";
        int j = foo(indices, levels);
        for (int i = j; i < indices.size(); i++) {
            helices.back().push_back(indices[i]);
            cout << indices[i] + 1 << ' ';
        }
        cout << endl;

        for (int i = 0; i < helices.back().size(); i++) {
            indices.pop_back();
            levels.pop_back();
        }
    };

    for (int i = 0; i < ss.size(); i++) {
        int l = ss_level(ss[i]);

        if (l == 0) {
            continue;
        }
        else if (l > 0) {
//            cout << "Symbol: " << ss[i] << endl;
//            cout << "Indices: "; for (auto && n : indices) cout << n << ' '; cout << endl;
//            cout << "Levels: "; for (auto && n : levels) cout << n << ' '; cout << endl;

            auto &indices = all_indices[l];
            auto &levels = all_levels[l];

            if (levels.empty()) {
                cerr << "Wrong secondary structure!" << endl;
                exit(1);
            } else {
                if (levels.back() > 0 && indices.back() + 1 != i) {
                    extract_helix(indices, levels);
                }

                indices.push_back(i);
                levels.push_back(l);

                int j = foo(indices, levels);
                if (j == 0 || indices[j - 1] + 1 != indices[j]) {
                    extract_helix(indices, levels);
                }
            }
        }
        else if (l < 0) {
//            cout << "Symbol: " << ss[i] << endl;
//            cout << "Indices: "; for (auto && n : indices) cout << n << ' '; cout << endl;
//            cout << "Levels: "; for (auto && n : levels) cout << n << ' '; cout << endl;

            auto &indices = all_indices[-l];
            auto &levels = all_levels[-l];

            if (!(levels.empty()) && levels.back() > 0) {
                extract_helix(indices, levels);
            }
            indices.push_back(i);
            levels.push_back(l);
        }
    }

    for (int i = 0; i < all_levels.size(); i++) {
        auto &indices = all_indices[i];
        auto &levels = all_levels[i];
        if (!indices.empty() || !levels.empty()) {
            cerr << "Wrong secondary structure!" << endl;
            exit(1);
        }
    }

    return move(helices);
}

double helix_i_ip1() {
    return 6.1;
}

double helix_i_ip2() {
    return 12.5;
}

double helix_i_ip(int n) {
    return std::sqrt(2 * 9.7*9.7*(1 - std::cos(0.562*n - 2 * PI)) + (2.84*n)*(2.84*n));
}

double helix_i_j() {
    return 15.1;
}

double helix_i_jm(int n) {
    return std::sqrt(2 * 9.7*9.7*(1 - std::cos(0.562*n - 0.5*PI)) + (2.84*n + 4)*(2.84*n + 4));
}

double helix_i_jp(int n) {
    return std::sqrt(2 * 9.7*9.7*(1 - std::cos(0.562*n - 1.5*PI)) + (2.84*n - 4)*(2.84*n - 4));
}

/**
 * Make a bound matrix from the secondary structure.
 */
Mat make_bound(const string &ss) {
    int l = ss.size();

    Mat bound(l, l);
    bound.set_values([](int i, int j){
        if (i == j) {
            return 0;
        }
        else if (i < j) {
            return 999;
        }
        else {
            return 3;
        }
    });

    // Set neighbouring (n - n+1) constraints
    for (int i = 0; i < l - 1; i++) {
        bound(i, i + 1) = bound(i + 1, i) = helix_i_ip(1);
    }

    // Set n - n+2 constraints
    for (int i = 0; i < l - 2; i++) {
        bound(i, i + 2) = bound(i + 2, i) = helix_i_ip(2);
    }

    // Set Helices
    auto && helices = identify_helices(ss);

    for (auto && helix : helices) {
        int sz = helix.size();
        int num_bps = helix.size() / 2;
        for (int i = 0; i < sz; i++) {
            for (int j = i + 1; j < sz; j++) {
                int a = helix[i];
                int b = helix[j];
                // i is in the 5' chain of the helix
                if (i < num_bps) {
                    // j is in the 5' chain of the helix
                    if (j < num_bps) {
                        bound(a, b) = bound(b, a) = helix_i_ip(j - i);
                    }
                    // j is in the 3' chain of the helix
                    else {
                        int ii = sz - 1 - i;
                        if (j == ii) {
                            bound(a, b) = bound(b, a) = helix_i_j();
                        }
                        else if (j < ii) {
                            bound(a, b) = bound(b, a) = helix_i_jm(ii - j);
                        }
                        else {
                            bound(a, b) = bound(b, a) = helix_i_jp(j - ii);
                        }
                    }
                }
                // i is in the 3' chain of the helix
                else {
                    bound(a, b) = bound(b, a) = helix_i_ip(j - i);
                }
            }
        }
    }

    return move(bound);
}

/**
 * Residue Pair.
 */
struct ResPair {
    int i;
    int j;
    double d;
    int cluster = -1;

    ResPair(int i_, int j_, double d_, int cluster_) :
        i(i_), j(j_), d(d_), cluster(cluster_)
    {}
};
using ResPairs = vector<ResPair>;

/**
 * Get pairs from contacts.
 */
ResPairs get_pairs(const Mat &bound, int max_num = -1) {
    ResPairs pairs;
    double cutoff = 3;
    for (int i = 0; i < bound.rows(); i++) {
        for (int j = i + 1; j < bound.cols(); j++) {
            double d = bound(i, j) - bound(j, i);
            if (d > cutoff) {
                pairs.push_back(ResPair{i, j, d, -1});
            }
        }
    }

    sort(pairs.begin(), pairs.end(), [](const ResPair &p1, const ResPair &p2){
        return p1.d > p2.d;
    });

    if (max_num != -1) {
        int n = pairs.size();
        for (int i = 0; i < n - max_num; i++) {
            pairs.pop_back();
        }
    }

    return move(pairs);
}

/**
 * Get pairs from a file.
 */
ResPairs get_pairs(const Mat &bound, const string &fname) {
    ifstream ifile(fname);
    string line;
    ResPairs pairs;
    while (ifile) {
        getline(ifile, line);
        auto v = string_tokenize(line, " \t");
        if (v.size() >= 2) {
            int i = parse<int>(v[0]) - 1;
            int j = parse<int>(v[1]) - 1;
            if (i > j) swap(i, j);
            double d = bound(i, j) - bound(j, i);
            pairs.push_back(ResPair{i, j, d, -1});
        }
    }
    ifile.close();

    sort(pairs.begin(), pairs.end(), [](const ResPair &p1, const ResPair &p2){
        return p1.d > p2.d;
    });

    return move(pairs);
}

/**
 * Set the cluster of pairs.
 */
int set_cluster(ResPairs &pairs, const Mat &b1, const Mat &b2, int cluster, double cutoff) {
    int num = 0;
    for (int n = 0; n < pairs.size(); n++) {
        int i = pairs[n].i;
        int j = pairs[n].j;
        double d = fabs((b2(i, j) - b2(j, i)) - (b1(i, j) - b1(j, i)));
        if (pairs[n].cluster == -1 && d > cutoff) {
            pairs[n].cluster = cluster;
            num++;
        }
    }
    return num;

//    int n = 0;
//    int num = 0;
//    for (int i = 0; i < b1.rows(); i++) {
//        for (int j = i + 1; j < b1.cols(); j++) {
//            double d = fabs((b2(i, j) - b2(j, i)) - (b1(i, j) - b1(j, i)));
//            if (pairs[n].cluster == -1 && d > cutoff) {
//                pairs[n].cluster = cluster;
//                num++;
//            }
//            n++;
//        }
//    }
//    return num;
}

using ResPairClusters = vector<vector<ResPair>>;

/**
 * Set clusters of pairs.
 */
ResPairClusters set_clusters(ResPairs &pairs, const Mat &b1) {
    vector<int> indices(pairs.size());
    iota(indices.begin(), indices.end(), 0);
    sort(indices.begin(), indices.end(), [&pairs](int i, int j){
        return pairs[i].d > pairs[j].d;
    });

    int cluster = 0;
    for (auto && n : indices) {
        if (pairs[n].cluster == -1) {
            auto b2 = b1;
            int i = pairs[n].i;
            int j = pairs[n].j;
            double u = b2(i, j);
            double l = b2(j, i);
            double d = l + (u - l) * my_rand();
            b2(i, j) = b2(j, i) = d;
            smooth(b2);
            pairs[n].cluster = cluster;
            set_cluster(pairs, b1, b2, cluster, (u - l) * 0.5);
            cluster++;
        }
    }

    cout << "Clustering: " << cluster << " Clusters." << endl;

    ResPairClusters clusters(cluster);

    for (int i = 0; i < pairs.size(); i++) {
        if (pairs[i].cluster != -1) {
            clusters[pairs[i].cluster].push_back(pairs[i]);
        }
    }

    for (int i = 0; i < cluster; i++) {
        sort(clusters[i].begin(), clusters[i].end(), [](const ResPair &p1, const ResPair &p2){
            return p1.d > p2.d;
        });
    }

    return move(clusters);
}

/**
 * Estimate the number of contacts based on the secondary structure.
 */
void estimate_contacts(const string &ss) {
    // Make bound matrix from sequence and secondary structure
    auto bound = make_bound(ss);
    smooth(bound);

    double dv_cutoff = 50.0;
    int n_attempts = 0;
    while (true) {
        n_attempts += 1;
        auto dv = find_max_dv(bound);

        double u = bound(dv[0], dv[1]);
        double l = bound(dv[1], dv[0]);
        double d = u - l;

//        if (d > dv_cutoff) {
        if (n_attempts < 50) {
            bound(dv[0], dv[1]) = bound(dv[1], dv[0]) = l + (u - l) * my_rand();
            smooth(bound);
        }
        else {
            break;
        }
    }

    double cutoff = 15.2;
    int n = 0;
    for (int i = 0; i < bound.rows(); i++) {
        for (int j = i + 1; j < bound.cols(); j++) {
            if (bound(j, i) < cutoff) {
                n += 1;
            }
        }
    }

    cout << "Estimated number of contacts: " << n << endl;

}

/**
 * Print the bound matrix.
 */
void print_bound(const Mat &bound) {
    /*
    if (flag == 0) return;

    int rows = bound.rows();
    int cols = bound.cols();
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            cout << bound(i, j) << ' ';
        }
        cout << endl;
    }
    cout << endl;
    */
}

/**
 * Select pairs from the secondary structure.
 */
void select_pairs(const string &ss, const string &ofname) {
    // Make bound matrix from sequence and secondary structure
    auto bound = make_bound(ss);
    print_bound(bound);

    smooth(bound);
    flag = 0;

    auto pairs = get_pairs(bound, 5000);

    auto clusters = set_clusters(pairs, bound);

    ofstream ofile(ofname.c_str());
    ofile << "#     Cluster      I         J  Distance Variance" << endl;
    for (int i = 0; i < clusters.size(); i++) {
        double sum = 0;
        for (int j = 0; j < clusters[i].size(); j++) {
            sum += clusters[i][j].d;
        }
        double avg = sum / clusters[i].size();
        if (avg != 0) {
//            ofile << "Cluster " << i + 1;
//            ofile << " (Average Distance Variation " << avg << ")" << endl;
            for (int j = 0; j < clusters[i].size(); j++) {
                ofile << string_format("%10d%10d%10d%12.3f", clusters[i][j].cluster + 1, clusters[i][j].i + 1, clusters[i][j].j + 1, clusters[i][j].d) << endl;
            }
        }
    }
    ofile.close();
}

/**
 * Rank all the pairs in a file according to their distance variation.
 */
void rank_pairs(const string &ss, const string &ifname, const string &ofname) {
    // Make bound matrix from sequence and secondary structure
    auto bound = make_bound(ss);

    smooth(bound);

    auto pairs = get_pairs(bound, ifname);

    cout << pairs.size() << " Pairs." << endl;

    auto clusters = set_clusters(pairs, bound);

    ofstream ofile(ofname.c_str());
    ofile << "#    I    J    Distance Variance   Cluster" << endl;
    for (int i = 0; i < pairs.size(); i++) {
        ofile << string_format("%10d%10d%10d%10d%12.3f", i + 1, pairs[i].i + 1, pairs[i].j + 1, pairs[i].d, pairs[i].cluster) << endl;
    }
    ofile.close();
}

void print_help() {
    cout << "USAGE:\n" << endl;
    cout << "[1] Compute DV of given residue pairs.\n" << endl;
    cout << "    dvass <ss> <ifname> <ofname>\n" << endl;
    cout << "    <ss> is the secondary structure; <ifname> is the input file name; <ofname> is the output file name.\n" << endl;
    cout << "    Example: dvass '((((....((((...))))..(((())))..))))' contacts.txt dv.txt\n" << endl;
    cout << "[2] Compute DV of top ranked residue pairs.\n" << endl;
    cout << "    dvass <ss> <ofname>\n" << endl;
    cout << "    <ss> is the secondary structure; <ofname> is the output file name.\n" << endl;
    cout << "    Example: dvass '((((....((((...))))..(((())))..))))' dv.txt\n" << endl;
    cout << "[3] Estimate the number of contacts based on the secondary structure.\n" << endl;
    cout << "    dvass contacts <ss>\n" << endl;
    cout << "    <ss> is the secondary structure.\n" << endl;
    cout << "    Example: dvass contacts '((((....((((...))))..(((())))..))))'\n" << endl;
}

int main(int argc, char **argv) {
    for (int i = 1; i < argc; i++) {
        string s(argv[i]);
        if (s == "-h" || s == "--help") {
            print_help();
            return 0;
        }
    }

    if (argc == 3) {
        if (string(argv[1]) == "contacts") {
            string ss(argv[2]);
            estimate_contacts(ss);
        }
        else {
            string ss(argv[1]);
            string ofname(argv[2]);
            select_pairs(ss, ofname);
        }
    }
    else if (argc == 4) {
        string ss(argv[1]);
        string ifname(argv[2]);
        string ofname(argv[3]);
        rank_pairs(ss, ifname, ofname);
    }
    else {
        print_help();
    }
    
//    for (int i = 0; i < 10; i++) {
//        cout << pairs[i].i << ' ' << pairs[i].j << ' ' << pairs[i].d << endl;
//    }

//    int l = ss.size();

//    ofstream ofile(ofname.c_str());
//    for (int i = 0; i < l; i++) {
//        for (int j = i + 1; j < l; j++) {
//            ofile << i + 1 << ' ' << j + 1 << ' ' << bound(j, i) << ' ' << bound(i, j) << endl;
//        }
//    }
//    ofile.close();
}

#undef PI

